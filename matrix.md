---
title: The matrix Module
keywords: matrix, row, column
sidebar: modules_sidebar
permalink: /modules/matrix/
toc: false
folder: Modules
author:
- 'Sophia Weeks'
---

## 2-dimensional matrix operations


The *matrix* module object can be imported using `import "matrix" as matrix`,
for any identifier `matrix` of your choice. 

`Matrix` objects represent 2-dimensional matrices. 
The object `matrix` responds to the following requests.

```
type Matrix⟦T⟧ = Collection⟦T⟧ & interface {
    size -> Number 
    // returns the number of values in the matrix
    numRows → Number
    // returns the number of rows in the matrix
    numColumns → Number 
    // returns the number of columns in the matrix
    atRow(r:Number) column(c:Number) put (v:T) → Matrix 
    // adds the value 'v' at row r, column c
    // raises BoundsError if index at r,c does not exist
    atRow(r:Number) column(c:Number) → T 
    // returns the value at row r, column c
    // raises BoundsError if index at r,c does not exist
    atRow(r:Number) column(c:Number) ifAbsent(action:Procedure0) → T 
    // returns the value at row r, column c
    // executes 'action' if index at r,c does not exist
    row(r:Number) → Collection⟦T⟧ 
    // returns the row at the specified index as a collection
    // raises BoundsError if index r does not exist
    column(c:Number) → Collection⟦T⟧
    // returns the column at the specified index as a collection
    // raises BoundsError if index c does not exist
    rows → Enumerable⟦Enumerable⟦T⟧⟧ 
    // returns an enumerable collection over the rows of the matrix
    columns → Enumerable⟦Enumerable⟦T⟧⟧ 
    // returns an enumerable collection over the columns of the matrix
    values → Enumerable⟦T⟧
    // returns an enumerable collection over the values of the matrix
    +(other:Matrix⟦T⟧) → Matrix⟦T⟧ 
    // returns the value-wise sum of two matrices
    // raises MatrixDimensionError if the dimensions of 'other' don't match the dimesions of 'self'
    -(other:Matrix⟦T⟧) → Matrix⟦T⟧ 
    // returns the value-wise difference of two matrices
    // raises MatrixDimensionError if the dimensions of 'other' don't match the dimesions of 'self'
    *(other) → Matrix⟦T⟧
    // returns the value-wise product if the argument is a matrix
    // returns the product of the value mapped to all values in the matrix if the argument is a scalar
    // raises MatrixDimensionError if the dimensions of 'other' don't match the dimesions of 'self'
    /(other) → Matrix⟦T⟧
    // returns the value-wise quotient if the argument is a matrix
    // returns the quotient of the value mapped to all values in the matrix if the argument is a scalar
    // raises MatrixDimensionError if the dimensions of 'other' don't match the dimesions of 'self'
    transpose → Matrix⟦T⟧
    // return the transpose of the matrix
    times(other:Matrix⟦T⟧) → Matrix⟦T⟧
    // returns the matrix product of the two matrices
    // raises MatrixDimensionError if the dimensions of 'other' are not compatible with the dimensions of 'self'
    reshapeWithNumRows(rs:Number) numColumns(cs:Number) → Matrix⟦T⟧
    // redefines the number of rows and columns in the matrix, if compatible with the number of values
    // raises MatrixDimensionError if the product of the new dimensions is not equal to the size of the values list
    reshapeWithNumRows(rs:Number) numColumns(cs:Number) 
        additionalValues(vs:Collection⟦T⟧) → Matrix⟦T⟧
    // redefines the number of rows and columns in the matrix, 
    // raises MatrixDimensionError if the product of the new dimensions 
    //   is not equal to the size of the values list plus the size of the additional values
    addRow(row:Collection⟦T⟧) at(index:Number) → Matrix⟦T⟧
    // adds a row to the matrix at the specified index
    // raises MatrixDimensionError if the length of the row is not equal to the number of rows in the matrix
    deleteRow(r:Number) → Matrix⟦T⟧
    // removes the row at the specified index from the matrix
    // raises BoundsError if the index r is not within the number of rows in the matrix
    addColumn(column:Collection⟦T⟧) at(index:Number) → Matrix⟦T⟧
    // adds a column to the matrix at the specified index
    // raises MatrixDimensionError if the length of the column is not equal to the number of columns in the matrix
    deleteColumn(c:Number) → Matrix⟦T⟧
    // removes the column at the specified index from the matrix
    // raises BoundsError if the index c is not within the number of columns in the matrix
    replaceRowAt(index:Number) with(row:Collection⟦T⟧) → Matrix⟦T⟧
    // replaces the row at the specified index with the given collection
    // raises MatrixDimensionError if the length of the row is not equal to the number of rows in the matrix
    // raises BoundsError if the index r is not within the number of rows in the matrix
    replaceColumnAt(index:Number) with(column:Collection⟦T⟧) → Matrix⟦T⟧
    // replaces the column at the specified index with the given collection
    // raises MatrixDimensionError if the length of the column is not equal to the number of columnss in the matrix
    // raises BoundsError if the index c is not within the number of columns in the matrix
    copy → Matrix⟦T⟧
    // returns a new matrix instance with the same values and dimensions as self
}
 
```